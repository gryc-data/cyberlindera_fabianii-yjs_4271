# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.2 (2021-05-18)

### Edited

* Delete all standard_name qualifiers.

## v1.1 (2020-10-06)

### Edited

* CDS CYFA0S_01e00100g turned into pseudo.
* CDS CYFA0S_04e00100g turned into pseudo.
* CDS CYFA0S_04e07074g turned into pseudo.
* CDS CYFA0S_06e05160g turned into pseudo.
* CDS CYFA0S_07e05204g turned into pseudo.
* CDS CYFA0S_14e00100g turned into pseudo.
* CDS CYFA0S_17e02432g turned into pseudo.
* CDS CYFA0S_29e01222g turned into pseudo.
* CDS CYFA0S_33e00914g turned into pseudo.
* CDS CYFA0S_38e00694g turned into pseudo.
* CDS CYFA0S_39e00474g turned into pseudo.
* CDS CYFA0S_41e00100g turned into pseudo.
* CDS CYFA0S_41e00342g turned into pseudo.
* Rename all scaffolds with the iGenolevures nomenclature.
* Edit 25 mRNA features to fit there corresponding CDS coordinates (for 24 => gap inclusion).
* Build locus hierarchy.

### Fixed

* locus CYFA0S_32e00826g: bad gene feature boundaries.

## v1.0 (2020-10-06)

### Added

* The 9 annotated chromosomes of Candida parapsilosis CDC 317 (source EBI, [GCA_000182765.2](https://www.ebi.ac.uk/ena/browser/view/GCA_000182765.2)).
