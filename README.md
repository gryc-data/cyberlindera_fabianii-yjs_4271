## *Cyberlindnera fabianii* YJS 4271

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEB6138](https://www.ebi.ac.uk/ena/browser/view/PRJEB6138)
* **Assembly accession**: [GCA_003205855.1](https://www.ebi.ac.uk/ena/browser/view/GCA_003205855.1)
* **Original submitter**: 

### Assembly overview

* **Assembly level**: Scaffold
* **Assembly name**: ASM320585v1
* **Assembly length**: 12,277,034
* **#Scaffolds**: 50
* **Mitochondiral**:  
* **N50 (L50)**: 486,206 (8)

### Annotation overview

* **Original annotator**: 
* **CDS count**: 5929
* **Pseudogene count**: 255
* **tRNA count**: 139
* **rRNA count**: 0
* **Mobile element count**: 0
